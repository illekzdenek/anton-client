import Vue from "vue";
import Vuex from "vuex";
import * as Constants from "./constants";
import * as LocalStorageHelper from "./helpers/LocalStorageHelper";
import * as Helper from "./helpers/MainHelper";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    options: {
      warningModal: true,
      maxFileSize: 1, //MB
      helpTooltips: true,
      mainScene: Constants.VIEW_HOME
    },
    alertBox: {
      display: false,
      message: "nfghjfg"
    },
    modal: {
      display: false,
      content: ""
    },
    activeView: {
      type: Constants.VIEW_LOGIN,
      history: [],
      goBackView: null
    },
    user: {
      name: "",
      password: "",
      institution: "",
      position: "",
      files: []
    },
    files: {
      accepted: [],
      rejected: [],
      queued: [],
      uploading: []
    },
    stagedFiles: {
      accepted: [],
      rejected: []
    },
    finishedFiles: [],
    predefined: {
      name: "Zdeněk Illek",
      email: "zdenek.illek@email.com",
      language: "en",
      keyword: "test",
      description: "testovací tag",
      year: new Date().getFullYear(),
      area: "Brno",
      url: "http://mendelu.cz/",
      iid: "nope"
    },
    sortingSettings: {
      perPage: 5,
      pageOptions: [5, 10, 15, 20],
      sortBy: "",
      sortDesc: false,
      sortDirection: "asc"
    },
    selectedComparingFile: {
      file: {}
    },
    selectedFileToCompare: {
      file: {}
    },
    activeRowData: {},
    selectedDocumentList: Constants.DOCUMENT_LIST_CURRENT_USER
  },
  mutations: {
    optionsInit(state, options) {
      state.options = options; //pro přidání nové option je potřeba toto zakomentovat v gui provést změnu v option a odkomentovat
    },
    updateOptions(state, params) {
      console.log(params);
      for (let f in state.options) {
        console.log(f);
        console.log(params);
        if (f === params.key) {
          state.options[f] = params.value;
        }
      }

      LocalStorageHelper.setItemToLocalStorage("options", state.options);
    },
    selectDocumentList(state, listName) {
      state.selectedDocumentList = listName;
    },
    displayAlertBox(state, message) {
      state.alertBox.display = true;
      state.alertBox.message = message;
    },
    hideAlertBox(state) {
      (state.alertBox.display = false), (state.alertBox.message = "");
    },
    displayModal(state, content) {
      state.modal.display = true;
      state.modal.content = content;
    },
    hideModal(state) {
      state.modal.display = false;
      state.modal.content = "";
    },
    logIn(state, params) {
      const user = params.user;
      state.user.name = user.name;
      state.user.password = user.pass;
      state.user.institution = user.institution;
      state.user.position = user.position;
      state.activeView.type = params.scene;
    },
    logOut(state) {
      state.user.name = "";
      state.user.password = "";
      state.user.institution = "";
      state.user.position = "";
      state.activeView.type = Constants.VIEW_LOGIN;
    },
    changeView(state, view) {
      const newHistory = state.activeView.history;
      newHistory.push(state.activeView.type);
      state.activeView.history = newHistory;
      state.activeView.goBackView = state.activeView.type;
      state.activeView.type = view;
    },
    goBackView(state) {
      if (state.activeView.goBackView !== null) {
        state.activeView.type = state.activeView.goBackView;
        state.activeView.history.pop();
        if (state.activeView.history.length !== 0) {
          state.activeView.goBackView =
            state.activeView.history[state.activeView.history.length - 1];
        } else {
          state.activeView.goBackView = null;
        }
      }
    },
    updateFileState(state, files) {
      (state.files.accepted = files.accepted),
        (state.files.rejected = files.rejected),
        (state.files.queued = files.queued),
        (state.files.uploading = files.uploading);
    },
    //objekt {uuid,info}
    updateFile(state, params) {
      for (let f of state.stagedFiles.accepted) {
        if (f.upload.uuid === params.uuid) {
          f.info = params.info;
        }
      }
      console.log(state.stagedFiles)
    },
    //pole nových souborů(base64 stringy)
    saveFile(state, file) {
      const newFinishedFiles = state.finishedFiles;
      newFinishedFiles.push(file);
      state.finishedFiles = newFinishedFiles;
    },
    loadSavedFiles(state, files) {
      state.finishedFiles = files;
    },
    clearSavedFiles(state) {
      const newFinishedFiles = [];
      state.finishedFiles = newFinishedFiles;
    },
    deleteFinishedFile(state, uuid) {
      LocalStorageHelper.setItemToLocalStorage("finishedFiles", []);
      let newFinishedFiles = [];
      for (let f of state.finishedFiles) {
        if (f.upload.uuid !== uuid) {
          newFinishedFiles.push(f);
          LocalStorageHelper.setFileToLocalStorage("finishedFiles", f);
        }
      }
      state.finishedFiles = newFinishedFiles;
    },
    updateSavedFile(state, params) {
      for (let f of state.finishedFiles) {
        if (f.upload.uuid === params.uuid) {
          LocalStorageHelper.updateFinishedFilesInLocalStorage(
            "finishedFiles",
            params.info
          );
          f.info = params.info;
        }
      }
    },
    //vymaže všechny files
    clearFiles(state) {
      state.files.accepted = [];
      state.files.rejected = [];
      state.files.queued = [];
      state.files.uploading = [];
      state.stagedFiles.accepted = [];
      state.stagedFiles.rejected = [];
    },
    clearRejected(state) {
      state.stagedFiles.rejected = [];
    },
    removeFile(state, info) {//info.fileState je deprecated
      let newFiles = [];
      if(info.fileState === "Staged-Accepted"){
        for (let f of state.stagedFiles.accepted) {
          if (f.upload.uuid !== info.uuid) {
            newFiles.push(f);
          }
        }
        state.stagedFiles.accepted = newFiles;
      }

      if(info.fileState === "Staged-Rejected"){
        for (let f of state.stagedFiles.rejected) {
          if (f.upload.uuid !== info.uuid) {
            newFiles.push(f);
          }
        }

        state.stagedFiles.rejected = newFiles;
      }
    },
    updatePredefined(state, predefined) {
      state.predefined = predefined;
    },
    updateSortingSettings(state, sortingOptions) {
      state.sortingSettings = sortingOptions;
    },
    setRowDataCompareFile(state, rowData) {
      state.activeRowData = rowData;
    },
    selectCompareFile(state, file) {
      state.selectedComparingFile.file = file;
    },
    selectFileToCompare(state, file) {
      state.selectedFileToCompare.file = file;
    },
    stageUploadedFile(state, file){
      if(file.state === "Accepted"){
        state.stagedFiles.accepted.push(file.file)
      }
      if(file.state === "Rejected"){
        state.stagedFiles.rejected.push(file.file)
      }
    },
    unstageUploadedFile(state, uuid){
      let newStagedFiles = [];
      for (let f of state.stagedFiles.accepted){
        if (uuid !== f.uploaded.uuid){
          newStagedFiles.push(f);
        }
      }
      for (let f of state.stagedFiles.rejected){
        if (uuid !== f.uploaded.uuid){
          newStagedFiles.push(f);
        }
      }

      state.stagedFiles = newStagedFiles;
    },
    unstageAll(state){
      state.stagedFiles.accepted = []
      state.stagedFiles.rejected = []
    }
  },
  actions: {
    optionsInit({ commit }, options) {
      commit("optionsInit", options);
    },
    updateOptions({ commit, dispatch }, params) {
      commit("updateOptions", params);
      dispatch("alertBox", "Option was succesfully changed.")
    },
    selectDocumentList({ commit }, listName) {
      commit("selectDocumentList", listName);
    },
    alertBox({ commit }, message) {
      commit("displayAlertBox", message);
      setTimeout(() => {
        commit("hideAlertBox");
      }, 3000);
    },
    displayModal({ commit }, content) {
      commit("displayModal", content);
    },
    hideModal({ commit }) {
      commit("hideModal");
    },
    logIn({ commit }, user) {
      commit("logIn", user);
    },
    logOut({ commit }) {
      commit("logOut");
    },
    changeView({ commit }, view = Constants.VIEW_HOME) {
      commit("changeView", view);
    },
    goBackView({ commit }) {
      commit("goBackView");
    },
    updateFileState({ commit }, files) {
      commit("updateFileState", files);
    },
    updateFile({ commit }, params) {
      commit("updateFile", params);
    },
    saveFile({ commit }, file) {
      commit("saveFile", file);
      LocalStorageHelper.setFileToLocalStorage("finishedFiles", file);
    },
    loadSavedFiles({ commit }, files) {
      commit("loadSavedFiles", files);
    },
    clearSavedFiles({ commit }) {
      commit("clearSavedFiles");
      LocalStorageHelper.setItemToLocalStorage("finishedFiles", []);
    },
    updateSavedFile({ commit }, params) {
      commit("updateSavedFile", params);
    },
    deleteFinishedFile({ commit }, uuid) {
      commit("deleteFinishedFile", uuid);
    },
    clearFiles({ commit }) {
      commit("clearFiles");
    },
    clearRejected({ commit }) {
      commit("clearRejected");
    },
    removeFile({ commit }, info) {
      commit("removeFile", info);
    },
    updatePredefined({ commit, state }, predefined) {
      if (predefined === null) {
        predefined = state.predefined;
      }
      commit("updatePredefined", predefined);
      LocalStorageHelper.setItemToLocalStorage("predefined", predefined);
    },
    updateSortingSettings({ commit, state }, sortingOptions) {
      if (sortingOptions === null) {
        sortingOptions = state.sortingSettings;
      }
      commit("updateSortingSettings", sortingOptions);
      LocalStorageHelper.setItemToLocalStorage(
        "sortingSettings",
        sortingOptions
      );
    },
    setRowDataCompareFile({ commit }, rowData) {
      commit("setRowDataCompareFile", rowData);
    },
    selectCompareFile({ commit }, file) {
      commit("selectCompareFile", file);
    },
    selectFileToCompare({ commit }, file) {
      commit("selectFileToCompare", file);
    },
    stageUploadedFile({ commit }, file) {
      commit("stageUploadedFile", file);
    },
    unstageUploadedFile({ commit }, file) {
      commit("unstageUploadedFile", file);
    },
    unstageAll({ commit }) {
      commit("unstageAll");
    }
  }
});
