import Store from "../store";
import * as LocalStorageHelper from "../helpers/LocalStorageHelper";
export const initialize = () => {
  const predefined = LocalStorageHelper.getItemFromLocalStorage("predefined");
  const sortingSettings = LocalStorageHelper.getItemFromLocalStorage(
    "sortingSettings"
  );
  const finishedFiles = LocalStorageHelper.getItemFromLocalStorage(
    "finishedFiles"
  );
  const options = LocalStorageHelper.getItemFromLocalStorage("options");

  //Store.dispatch("clearSavedFiles")
  Store.dispatch("updatePredefined", predefined);
  Store.dispatch("updateSortingSettings", sortingSettings);
  if (finishedFiles !== null) {
    console.log(finishedFiles);
    Store.dispatch("loadSavedFiles", finishedFiles);
  }

  if (options) {
    Store.dispatch("optionsInit", options);
  }
};
