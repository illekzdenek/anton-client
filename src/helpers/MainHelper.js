import { Base64 } from "js-base64";
import Store from "../store";
import * as Constants from "../constants";

export function megaBytesToKiloBites(megaBytes) {
  return megaBytes * 1000;
}
export function getKiloBites(bites) {
  return bites / 1000;
}

export function getFileFormat(fileName) {
  let names = fileName.split(".");
  return names[1];
}

export function getFileName(fileName) {
  let names = fileName.split(".");
  return names[0];
}

export function getDateTime(date) {
  let d, m, y;
  if (typeof date === "string") {
    let fromString = new Date(date);
    d = fromString.getDate();
    m = fromString.getMonth() + 1;
    y = fromString.getFullYear();
    return d + "." + m + "." + y;
  }
  d = date.getDate();
  m = date.getMonth() + 1;
  y = date.getFullYear();
  return d + "." + m + "." + y;
}

export async function fileToBase64(file) {
  function getBase64(file) {
    const reader = new FileReader();
    return new Promise(resolve => {
      reader.onload = ev => {
        resolve(ev.target.result);
      };
      reader.readAsDataURL(file);
    });
  }

  return await getBase64(file);
}

export async function saveFileBlobs(fileList) {
  const base64Code = await fileToBase64(fileList);
  return b64EncodeUnicode(base64Code);
}

export function getDecodedData(base64String) {
  let decodedString = b64DecodeUnicode(base64String);
  let data = decodedString.split(",");
  return atob(data[1]);
}
export function b64EncodeUnicode(str) {
  let result = Base64.encode(str);
  return result;
}
export function b64DecodeUnicode(str) {
  let result = Base64.decode(str);
  return result;
}
export function getFileList() {
  //funkce pro náhodné zařazení souborů do listů (pro demonstrační účely)
  const randomNumber = Math.round(Math.random() * 3);
  return randomNumber;
}
export function getListItems(listId) {
  const listItems = [];
  for (let item of Store.state.finishedFiles) {
    if (item.info.list === listId) listItems.push(item);
  }

  return listItems;
}

export function getNavigationPathFromActiveScene(scene) {
  switch (scene) {
    case Constants.VIEW_HOME:
      return Constants.HIERARCHY_HOME;
    case Constants.VIEW_LOGIN:
      return Constants.HIERARCHY_LOGIN;
    case Constants.VIEW_ADD_NEW_DOCUMENT:
      return Constants.HIERARCHY_ADD_NEW_DOCUMENT;
    case Constants.VIEW_ADD_NEW_DOCUMENT_INFO:
      return Constants.HIERARCHY_ADD_NEW_DOCUMENT_INFO;
    case Constants.VIEW_ADD_SETTINGS:
      return Constants.HIERARCHY_ADD_SETTINGS;
    case Constants.VIEW_ADD_SETTINGS_PREFFIL:
      return Constants.HIERARCHY_ADD_SETTINGS_PREFFIL;
    case Constants.VIEW_ADD_SETTINGS_OPTIONS:
      return Constants.HIERARCHY_ADD_SETTINGS_OPTIONS;
    case Constants.VIEW_ADD_SETTINGS_SORT:
      return Constants.HIERARCHY_ADD_SETTINGS_SORT;
    case Constants.VIEW_MANAGE_FILES:
      return Constants.HIERARCHY_MANAGE_FILES;
    case Constants.VIEW_MANAGE_FILES_DETAIL:
      return Constants.HIERARCHY_MANAGE_FILES_DETAIL;
  }
}
