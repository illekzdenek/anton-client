export const setItemToLocalStorage = (key, item) => {
  console.log("Inserting ");
  console.log(item);
  window.localStorage.setItem(key, JSON.stringify(item));
};
export const getItemFromLocalStorage = key => {
  console.log("Getting item from " + key + " key.");
  return JSON.parse(window.localStorage.getItem(key));
};
export const removeItemFromLocalStorage = key => {
  try {
    window.localStorage.removeItem(key);
    console.log("Succesfuly removed " + key + ".");
    return true;
  } catch (error) {
    console.log("Error: " + error + ".");
    return false;
  }
};
export const clearStorage = () => {
  try {
    window.localStorage.clear();
    console.log("Succesfuly cleared");
    return true;
  } catch (error) {
    console.log("Error: " + error + ".");
    return false;
  }
};
export const setFileToLocalStorage = (key, item) => {
  let filesFromStorage = getItemFromLocalStorage(key) !== null ? getItemFromLocalStorage(key): [];
  filesFromStorage.push(item);
  window.localStorage.setItem(key, JSON.stringify(filesFromStorage));
};
export const updateFinishedFilesInLocalStorage = (key, info) => {
  let filesFromStorage = getItemFromLocalStorage(key) !== null ? getItemFromLocalStorage(key): [];
  let updatedFiles = [];
  for (let f of filesFromStorage) {
    if (f.upload.uuid === info.uuid) {
      let updatedFile = f;
      updatedFile.info = info;
      updatedFiles.push(updatedFile);
    }
    if (f.upload.uuid !== info.uuid) {
      updatedFiles.push(f);
    }
  }
  setItemToLocalStorage(key, updatedFiles);
};
