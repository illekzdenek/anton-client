//View Types
export const VIEW_HOME = "Home";
export const VIEW_LOGIN = "Login";
export const VIEW_ADD_NEW_DOCUMENT = "New_document";
export const VIEW_ADD_NEW_DOCUMENT_INFO = "New_document_info";
export const VIEW_ADD_SETTINGS = "Settings";
export const VIEW_ADD_SETTINGS_PREFFIL = "Settings_preffil";
export const VIEW_ADD_SETTINGS_OPTIONS = "Settings_options";
export const VIEW_ADD_SETTINGS_SORT = "Settings_sorting";
export const VIEW_MAIN_LAYOUT = "Main_Layout";
export const VIEW_MANAGE_FILES = "Manage_files";
export const VIEW_MANAGE_FILES_DETAIL = "Manage_files_detail";

export const DOCUMENT_LIST_CURRENT_USER = 0;
export const DOCUMENT_LIST_ERROR = 1;
export const DOCUMENT_LIST_SUSPISIOUS = 2;
export const DOCUMENT_LIST_SOURCE_CODE = 3;

//alertBox types
export const ALERTBOX_INFO = "info";

//settings
export const AREA = "Brno";
export const YEAR = new Date().getFullYear();

//hieararchy
export const HIERARCHY_HOME = "Home";
export const HIERARCHY_LOGIN = "Login";
export const HIERARCHY_ADD_NEW_DOCUMENT = "Home / New_document";
export const HIERARCHY_ADD_NEW_DOCUMENT_INFO =
  "Home / New_document / New_document_info";
export const HIERARCHY_ADD_SETTINGS = "Home / Settings";
export const HIERARCHY_ADD_SETTINGS_PREFFIL =
  "Home / Settings / Settings_preffil";
export const HIERARCHY_ADD_SETTINGS_OPTIONS =
  "Home / Settings / Settings_options";
export const HIERARCHY_ADD_SETTINGS_SORT = "Home / Settings / Settings_sorting";
export const HIERARCHY_MANAGE_FILES = "Home / Manage_files";
export const HIERARCHY_MANAGE_FILES_DETAIL =
  "Home / Manage_files / Manage_files_detail";
