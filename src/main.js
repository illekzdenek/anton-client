import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Highlighter from "vue-highlight-words";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCog,
  faHome,
  faArrowLeft,
  faSearchMinus,
  faFileAlt,
  faTimes,
  faUserSecret,
  faCopy,
  faFileSignature,
  faArrowRight,
  faSave,
  faEyeSlash,
  faEye,
  faUpload
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(
  faCog,
  faHome,
  faArrowLeft,
  faTimes,
  faFileAlt,
  faSearchMinus,
  faCopy,
  faFileSignature,
  faArrowRight,
  faSave,
  faEyeSlash,
  faEye,
  faUpload
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("Highlighter", Highlighter);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
