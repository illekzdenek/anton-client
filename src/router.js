import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Store from "./store";
import * as BootService from "./services/BootService";

Vue.use(Router);
BootService.initialize();

export default new Router({
  routes: [
    /* {
      path: "/",
      name: "home",
      component: Home
    },*/
    {
      path: "/",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Login.vue")
    },
    {
      path: "/main",
      name: "main",
      beforeEnter: (to, from, next) => {
        if (Store.state.user.name === "") next("/");
        else next();
      },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Main.vue")
    }
  ]
});
